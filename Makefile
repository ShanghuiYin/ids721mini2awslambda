
watch:
	cargo lambda watch

invoke:
	cargo lambda invoke --data-ascii "{ \"x\": 1, \"y\": 2 }"

build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-2

aws-invoke:
	cargo lambda invoke --remote new-lambda-project --data-ascii "{ \"x\": 1, \"y\": 2 }"

