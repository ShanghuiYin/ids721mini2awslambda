use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

/**
 * simple add function which takes in two digit and return request id and sum result
 */

#[derive(Deserialize)]
struct Request {
    x: i32,
    y: i32,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    total: i32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let x = event.payload.x;
    let y = event.payload.y;

    let total = x + y;

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        total,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        //.with_max_level((tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
