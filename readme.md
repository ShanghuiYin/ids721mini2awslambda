# IDS721 Spring 2024 Weekly Mini Project 2

## Requirements
1. Rust Lambda Function using Cargo Lambda.
2. Process and transform sample data

## Setup
1. Install Rust with cargo
2. Install AWS Toolkit for VS Code
3. Install Cargo Lambda `brew tap cargo-lambda/cargo-lambda
   brew install cargo-lambda`

## Run
My simple lambda function takes two numbers and returns the request id and sum.

1. Create the project `cargo lambda new new-lambda-project \
   && cd new-lambda-project`
2. Serve the function locally for testing `cargo lambda watch`
3. Test `cargo lambda invoke --data-ascii "{ \"x\": 1, \"y\": 2 }"`
4. Output `{"req_id":"09dc7f18-120e-4ba5-99f4-29e19a6dde50","total":3}`

## Deploy
1. Deploy the function `cargo lambda deploy`
2. Invoke the function `cargo lambda invoke --remote new-lambda-project --data-ascii "{ \"x\": 1, \"y\": 2 }"`

As shown in the screenshot below, the function is deployed and invoked successfully.

![img.png](img.png)

## AWS API Gateway Trigger

1. Open the AWS Management Console
2. Click on the lambda function service
3. Select and Click the function you want to add the trigger
4. Click on the Add trigger button
5. Choose the API Gateway
6. Choose the Create a new API option
7. Choose the REST API option
8. Choose Security Method _Open_
9. Click on the Add button
10. Access the function using the URL provided in the API Gateway trigger details


## Reference

- https://www.cargo-lambda.info/
- https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html
